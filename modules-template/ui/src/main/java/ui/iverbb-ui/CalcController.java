package iverbb.ui;

import core.Calc;

import java.util.List;
import java.util.function.BinaryOperator;
import java.util.function.UnaryOperator;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.Labeled;
import javafx.scene.control.ListView;

public class CalcController {

    private Calc calc;

    public CalcController() {
        calc = new Calc(0.0, 0.0, 0.0);
    }

    public Calc getCalc() {
        return calc;
    }

    public void setCalc(Calc calc) {
        this.calc = calc;
        updateOperandsView();
    }

    @FXML
    private ListView<Double> operandsView;

    @FXML
    private Label operandView;

    @FXML
    void initialize() {
        updateOperandsView();
    }

    private void updateOperandsView() {
        List<Double> operands = operandsView.getItems();
        operands.clear();
        int elementCount = Math.min(calc.getOperandCount(), 3);
        for (int i = 0; i < elementCount; i++) {
            operands.add(calc.peekOperand(elementCount - i - 1));
        }
    }

    private String getOperandString() {
        return operandView.getText();
    }

    private boolean hasOperand() {
        return ! getOperandString().isBlank();
    }

    private double getOperand() {
        return Double.valueOf(operandView.getText());
    }
    
    private void setOperand(String operandString) {
        operandView.setText(operandString);
    }

    @FXML
    void handleEnter() {
        if (hasOperand()) {
            calc.pushOperand(getOperand());
        } else {
            calc.dup();
        }
        setOperand("");
        updateOperandsView();
    }

    private void appendToOperand(String s) {
        // TODO
        setOperand(String.join("", getOperandString(), s));
    }

    @FXML
    void handleDigit(ActionEvent ae) {
        if (ae.getSource() instanceof Labeled l) {
            // TODO append button label to operand
            appendToOperand(l.getText());
        }
    }

    @FXML
    void handlePoint() {
        var operandString = getOperandString();
        if (operandString.contains(".")) {
            //while (Calc.peek) {
            // TODO remove characters after point
        } else {
            appendToOperand(".");
            // TODO append point
        }
    }

    @FXML
    void handleClear() {
        // TODO clear operand
        setOperand("");
    }

    @FXML
    void handleSwap() {
        // TODO clear operand
        handleClear();
        calc.swap();
        updateOperandsView();
    }

    private void performOperation(UnaryOperator<Double> op) {
        // TODO
        calc.performOperation(op);
        updateOperandsView();
    }

    private void performOperation(boolean swap, BinaryOperator<Double> op) {
        if (hasOperand()) {
            // TODO push operand first
            calc.pushOperand(getOperand());
        }
        // TODO perform operation, but swap first if needed
        if (swap) calc.swap();
        calc.performOperation(op);
        updateOperandsView();
    }

    @FXML
    void handleOpAdd() {
        // TODO
        BinaryOperator<Double> add = (a, b) -> a + b;
        performOperation(false, add);
    }

    @FXML
    void handleOpSub() {
        // TODO
        BinaryOperator<Double> sub = (a, b) -> a - b;
        performOperation(false, sub);
    }

    @FXML
    void handleOpMult() {
        // TODO
        BinaryOperator<Double> mult = (a, b) -> a * b;
        performOperation(false, mult);
    }

    @FXML
    void handleOpDiv() {
        // TODO
        BinaryOperator<Double> div = (a, b) -> a / b;
        performOperation(true, div);
    }

    @FXML
    void handleOpSqrt() {
        // TODO
        UnaryOperator<Double> sqrt = (a) -> Math.sqrt(a);
        performOperation(sqrt);
    }

    @FXML
    void handlePI() {
        // TODO
        calc.pushOperand(Math.PI);
        updateOperandsView();
    }
}
